module gitlab.com/nekom/tracker

go 1.13

require (
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/golang/protobuf v1.3.2
	github.com/grpc-ecosystem/grpc-gateway v1.11.3
	github.com/jinzhu/inflection v1.0.0 // indirect
	google.golang.org/genproto v0.0.0-20191009194640-548a555dbc03
	google.golang.org/grpc v1.24.0
	mellium.im/sasl v0.2.1 // indirect
)
