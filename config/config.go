package config

import (
	"context"
)

// Config for running GRPC/REST server
type Config struct {
	IP   string `json:"ip"`
	Port int    `json:"port"`
}

// Server implement ConfigServiceServer
type Server struct{}

// Grpc config request
func (c *Server) Grpc(ctx context.Context, req *ConfigRequest) (*ConfigResponse, error) {
	// Restart GRPC server

	return &ConfigResponse{}, nil
}

// Rest config request
func (c *Server) Rest(ctx context.Context, req *ConfigRequest) (*ConfigResponse, error) {
	// Restart REST server

	return &ConfigResponse{}, nil
}
