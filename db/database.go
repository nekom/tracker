package db

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

// Item in the cart
type Item struct {
	ItemNumber string  `json:"itemnumber"`
	Quantity   int     `json:"quantity"`
	Price      float32 `json:"price"`
}

// Order request
type Order struct {
	CustomerID string    `sql:"customerid,pk"`
	OrderID    string    `sql:"orderid,pk"`
	SiteID     string    `sql:"siteid,pk"`
	Cart       []Item    `pg:",array`
	Time       time.Time `pg:"type:timestamptz"`
}

// DB handler
var DB *pg.DB
var dbHost = "localhost"
var dbPort = "5432"

// Connect the database
func Connect() {
	if host := os.Getenv("DB_HOST"); host != "" {
		dbHost = host
	}

	if port := os.Getenv("DB_PORT"); port != "" {
		dbPort = port
	}

	opts := pg.Options{
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASS"),
		Database: os.Getenv("DB_NAME"),
		Addr:     fmt.Sprintf("%s:%s", dbHost, dbPort),
	}

	// Wait forever for the DB connection
	// There is no easy way to find out whether pg.Connect() successfully
	// connected to DB or not.
	for {
		DB = pg.Connect(&opts)
		if DB == nil {
			log.Println("Error connecting to database")
		}

		// Create tables if they don't exist.
		err := CreateTables()
		if err != nil {
			log.Println(err)
			time.Sleep(5 * time.Second)
		} else {
			// Connected
			return
		}
	}

}

// CreateTables creates tables if they don't exists
func CreateTables() error {
	opts := orm.CreateTableOptions{
		IfNotExists: true,
	}

	if err := DB.CreateTable(&Order{}, &opts); err != nil {
		return err
	}
	return nil
}

// InsertOrder into the database
func InsertOrder(ctx context.Context, order *Order) error {
	return DB.Insert(order)
}

// OrderStats returns number of times the item was ordered within two timestamps
func OrderStats(ctx context.Context, itemnumber string, t1, t2 time.Time) (int, int, float32, error) {
	var totalOrders, totalQuantity int
	var totalPrice float32

	var orders []Order
	err := DB.Model(&orders).Where("time >= ? and time <= ?", t1, t2).Select()
	if err != nil {
		return 0, 0, 0, err
	}

	for _, order := range orders {
		for _, item := range order.Cart {
			if itemnumber == item.ItemNumber {
				totalOrders++
				totalQuantity += item.Quantity
				totalPrice += item.Price
			}
		}
	}
	return totalOrders, totalQuantity, totalPrice, nil
}
