package main

import (
	"context" // Use "golang.org/x/net/context" for Golang version <= 1.6
	"net"
	"net/http"
	"os"

	"github.com/golang/glog"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	db "gitlab.com/nekom/tracker/db"
	svc "gitlab.com/nekom/tracker/service"
)

var grpcAddr = "localhost:9090"
var restAddr = "localhost:8080"

func startGrpcServer() {
	// Connect and initialize the database
	db.Connect()

	if os.Getenv("LISTERNER_ADDR_GRPC") != "" {
		grpcAddr = os.Getenv("LISTERNER_ADDR_GRPC")
	}

	listener, err := net.Listen("tcp", grpcAddr)
	if err != nil {
		panic(err)
	}
	srv := grpc.NewServer()
	svc.RegisterTrackerServiceServer(srv, &svc.TrackerServer{})
	reflection.Register(srv)

	if err := srv.Serve(listener); err != nil {
		panic(err)
	}
}

func startRestServer() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Register gRPC server endpoint
	// Note: Make sure the gRPC server is running properly and accessible
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := svc.RegisterTrackerServiceHandlerFromEndpoint(ctx, mux, grpcAddr, opts)
	if err != nil {
		return err
	}

	if os.Getenv("LISTENER_ADDR_REST") != "" {
		restAddr = os.Getenv("LISTENER_ADDR_REST")
	}

	// Start HTTP server (and proxy calls to gRPC server endpoint)
	return http.ListenAndServe(restAddr, mux)
}

func main() {
	defer glog.Flush()

	// Start GRPC server at localhost:9000
	go startGrpcServer()

	// Start REST server at localhost:9090
	if err := startRestServer(); err != nil {
		glog.Fatal(err)
	}
}
