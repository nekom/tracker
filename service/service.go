package service

import (
	"context"
	"time"

	"gitlab.com/nekom/tracker/db"
)

// TrackerServer implement TrackerServiceServer
type TrackerServer struct{}

// Track order request
func (t *TrackerServer) Track(ctx context.Context, req *TrackRequest) (*TrackResponse, error) {
	order := &db.Order{
		CustomerID: req.GetCustomerid(),
		OrderID:    req.GetOrderid(),
		SiteID:     req.GetSiteid(),
		// Don't store nanoseconds
		Time: time.Unix(time.Now().Unix(), 0),
	}

	for _, item := range req.GetCart() {
		order.Cart = append(order.Cart,
			db.Item{
				ItemNumber: item.Itemnumber,
				Quantity:   int(item.Quantity),
				Price:      item.Price,
			})
	}

	if err := db.InsertOrder(ctx, order); err != nil {
		return nil, err
	}
	return &TrackResponse{}, nil
}

// Stats of order requests
func (t *TrackerServer) Stats(ctx context.Context, req *StatsRequest) (*StatsResponse, error) {
	totalOrders, totalQuantity, totalPrice, err := db.OrderStats(ctx,
		req.Itemnumber,
		time.Unix(req.Starttime.Seconds, 0),
		time.Unix(req.Endtime.Seconds, 0))
	if err != nil {
		return nil, err
	}
	return &StatsResponse{
		Totalorders:   int32(totalOrders),
		Totalquantity: int32(totalQuantity),
		Totalprice:    totalPrice,
	}, nil
}
