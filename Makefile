all:
	CGO_ENABLED=0 go build -a -installsuffix cgo -o tracker .

docker:
	docker build -t tracker .

clean:
	rm -f tracker
