FROM golang:1.13-alpine
WORKDIR /go/src/tracker
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

EXPOSE 8080
EXPOSE 9090

CMD ["tracker"]
