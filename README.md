# tracker

## Story:
As a user of the microservice I want to be able to send website tracking information to this service via POST URL. Via GRPC Call I statistics information from the service.
 
## Dev Hints:
The POST Body should include maximum these parameters:
```
{
	“customerid”:string, <mandatory>
	“orderid”:string,
	“siteid”:string <mandatory>
	“cart”:[{“itemnumber”:string, “quantity”:int,”price”:float}]
}
```

* The service should store this information in its own database. Store every record with timestamp please
* Via GRPC it should be possible to show how often an itemnumber was in carts in a specific daterange.
* Via gitlab.yml we create a docker container during deployment and deliver a ready dockercontainer including database and service.
* The configuration of the service is done via json outside the docker container.  We need to configure on which port the service will listen for grpc and for rest and on which ip it will listen in my opinion. And this configuration should be configurable outside of the container to make it possible to change without deploy.
